'use strict'

module.exports = soonish

function soonish (min, max, fn) {
  var wait
  if (typeof max !== 'number') {
    fn = max
    wait = random(min * 0.5, min)
  } else {
    wait = random(min, max)
  }
  if (typeof fn !== 'function') {
    return (...args) => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(...args)
        }, wait)
      })
    }
  } else {
    setTimeout(fn, wait)
  }
}

function random (min, max) {
  return Math.round(min + Math.random() * (max - min))
}
