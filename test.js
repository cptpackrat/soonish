'use strict'

const soonish = require('./index.js')
const expect = require('chai').expect

describe('soonish', () => {
  it('executes callback if one is provided', (done) => {
    soonish(100, () => {
      soonish(0, 100, () => {
        done()
      })
    })
  })
  it('returns promisable function if no callback is provided', (done) => {
    Promise.resolve()
      .then(soonish(100))
      .then(soonish(0, 100))
      .then(done)
  })
  it('passes any received values to the next promise in the chain', (done) => {
    Promise.resolve(1234)
      .then(soonish(100))
      .then(soonish(0, 100))
      .then((val) => {
        expect(val).to.equal(1234)
        done()
      })
  })
  context('when both \'min\' and \'max\' are provided', () => {
    it('waits at least \'min\' milliseconds when used with callbacks', (done) => {
      var t1 = new Date()
      soonish(100, 200, () => {
        var t2 = new Date()
        expect(t2 - t1).to.be.at.least(100)
        done()
      })
    })
    it('waits at least \'min\' milliseconds when used with promises', (done) => {
      var t1 = new Date()
      Promise.resolve()
        .then(soonish(100, 200))
        .then(() => {
          var t2 = new Date()
          expect(t2 - t1).to.be.at.least(100)
          done()
        })
    })
  })
  context('when only \'max\' is provided', () => {
    it('waits at least \'max * 0.5\' milliseconds when used with callbacks', (done) => {
      var t1 = new Date()
      soonish(200, () => {
        var t2 = new Date()
        expect(t2 - t1).to.be.at.least(100)
        done()
      })
    })
    it('waits at least \'max * 0.5\' milliseconds when used with promises', (done) => {
      var t1 = new Date()
      Promise.resolve()
        .then(soonish(200))
        .then(() => {
          var t2 = new Date()
          expect(t2 - t1).to.be.at.least(100)
          done()
        })
    })
  })
})
