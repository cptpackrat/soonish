Soonish
=======
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

Sometimes you just need to chill for a second.

## Installation
```
npm install soonish
```

## Usage

### soonish(min, max, [callback])
Execute the provided callback after a random delay between `min` and `max` milliseconds. If `callback` is omitted, a function will be returned that can be used in a promise chain to achieve the same effect.

### soonish(max, [callback])
Execute the provided callback after a random delay between `max * 0.5` and `max` milliseconds. If `callback` is omitted, a function will be returned that can be used in a promise chain to achieve the same effect.

## Callback Example
```js
#!/usr/bin/env node
const soonish = require('soonish')
const debug = require('debug')('test')

debug('before')
soonish(0, 500, () => {
  debug('after first')
  soonish(500, () => {
    debug('after second')
  })
})
```
```
$ DEBUG=test ./example.js
  test before +0ms
  test after first +172ms
  test after second +282ms
```

## Promised Example
```js
#!/usr/bin/env node
const soonish = require('soonish')
const debug = require('debug')('test')

Promise.resolve()
  .then(() => { debug('before') })
  .then(soonish(0, 500))
  .then(() => { debug('after first') })
  .then(soonish(500))
  .then(() => { debug('after second') })
```
```
$ DEBUG=test ./example.js
  test before +0ms
  test after first +140ms
  test after second +309ms
```

Any values received will be passed through to the next promise in the chain:
```js
#!/usr/bin/env node
const soonish = require('soonish')

Promise.resolve('hello, world!')
  .then(soonish(500))
  .then(console.log)
```
```
$ ./example.js
hello, world!
```

[npm-image]: https://img.shields.io/npm/v/soonish.svg
[npm-url]: https://www.npmjs.com/package/soonish
[pipeline-image]: https://gitlab.com/cptpackrat/soonish/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/soonish/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/soonish/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/soonish/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
